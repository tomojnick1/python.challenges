import random


def coin_flip():
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"


def start():
    trial = 0
    choose_heads = False
    choose_tails = False
    while not (choose_tails and choose_heads):
        trial += 1
        if coin_flip() == "heads":
            choose_heads = True
        else:
            choose_tails = True
    return trial


total_trail = 0
for _ in range(0, 10000):
    total_trail += start()

average = total_trail / 10000

print(f"Result is: {average}")
