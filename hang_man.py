import random

lista = ["cs", "kts"]
wybor_slowa = random.choice(lista)
masked_word = "_" * len(wybor_slowa)
litera_zgadnieta = False

print(masked_word)


def guess_word(litera):
    new_masked_word = ""
    for char in wybor_slowa:
        if char == litera:
            new_masked_word += litera
            litera_zgadnieta = True

        else:
            print("Nie zgadłes litery, tracisz punkt")
    masked_word = new_masked_word
