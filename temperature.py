def convert_cel_to_far(C):
    F = C * 9 / 5 + 32
    return F


C = float(input("Enter a temperature in degrees C: "))
temp_F = convert_cel_to_far(C)


def convert_far_to_cel(F):
    C = (F - 32) * 5 / 9
    return C


F = float(input("Enter a temperature in degrees F: "))
temp_C = convert_far_to_cel(F)

print(f"{C} degrees F = {temp_F:.2f} degrees C")
print(f"{F} degrees C = {temp_C:.2f} degrees F")
