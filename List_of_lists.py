universities = [
    ["California Institute of Technology", 2175, 37704],
    ["Harvard", 19627, 39849],
    ["Massachusetts Institute of Technology", 10566, 40732],
    ["Princeton", 7802, 37000],
    ["Rice", 5879, 35551],
    ["Stanford", 19535, 40569],
    ["Yale", 11701, 40500],
]


def enrollment_stats(universities):
    enrolled_students = []
    tuition_fees = []

    for university in universities:
        enrolled_students.append(university[1])
        tuition_fees.append(university[2])
    return enrolled_students, tuition_fees


def mean(values):
    return sum(values) / len(values)


def median(values):
    sorted_values = sorted(values)
    length = len(values)

    if length % 2 == 0:
        middle1 = sorted_values[length // 2 - 1]
        middle2 = sorted_values[length // 2]
        return (middle1 + middle2) / 2
    else:
        return sorted_values[length // 2]


students, fees = enrollment_stats(universities)
total_students = sum(students)
total_tuition = sum(fees)
mean_students = mean(students)
median_students = median(students)
mean_tuition = mean(fees)
median_tuition = median(fees)


print("******************************")
print(f"Total students: {total_students}")
print(f"Total tuition: $ {total_tuition:,}")
print(f"Student mean: {mean_students:,.2f}")
print(f"Student median: {median_students}")
print(f"Tuition mean: $ {mean_tuition:,.2f}")
print(f"Tuition median: $ {median_tuition:,}")
print("******************************")
