import random


def trial():
    win_count = 0
    chances = [0.87, 0.65, 0.17]

    for chance in chances:
        if random.random() < chance:
            win_count += 1

    if win_count >= 2:
        return True
    else:
        return False


total_wins = 0
for _ in range(10000):
    total_wins += trial()

average = total_wins / 10000

print(f"Wins in: {average}")
