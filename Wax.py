import random

nouns = [
    "fossil",
    "horse",
    "aardvark",
    "judge",
    "chef",
    "mango",
    "extrovert",
    "gorilla",
]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
prepositions = [
    "against",
    "after",
    "into",
    "beneath",
    "upon",
    "for",
    "in",
    "like",
    "over",
    "within",
]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]

noun1, noun2, noun3 = random.sample(nouns, 3)
verb1, verb2, verb3 = random.sample(verbs, 3)
adj1, adj2, adj3 = random.sample(adjectives, 3)
prep1, prep2 = random.sample(prepositions, 2)
adverb1 = random.choice(adverbs)


line1 = f"A {adj1} {noun1}"
line2 = f"A {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}"
line3 = f"{adverb1}, the {noun1} {verb2}"
line4 = f"the {noun2} {verb3} {prep2} a {adj3} {noun3}"


poem = f"{line1}\n{line2}\n{line3}\n{line4}"
print(poem)
