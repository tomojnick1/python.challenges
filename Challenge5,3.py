def calc(base, exponent):
    result = pow(base, exponent)
    print(f"{base} to the power of {exponent} = {result:.4f}")


base = float(input("Enter a base: "))
exponent = int(input("Enter a exponent: "))

calc(base, exponent)
